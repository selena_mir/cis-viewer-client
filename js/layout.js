function Layout(canvas, height = 500, width = 500) {
    this._canvasHeight = height;
    this._canvasWidth = width;
    this.apply = function () {
        /***********links************/
        var links = d3.selectAll('.link');
        var linksCount = links._groups[0].length;
        var dataLinks = this._getDataAboutLinks();
        var linkForce = d3.forceLink(dataLinks)
            .id(function (d) {/*set node property in "dataNodes" which value
             corresponds "source" and "target" in "dataLinks"*/
                return d.id;
            })
            .distance(function () { /*set distance between linked nodes*/
                return 100;
            })
        links.data(dataLinks);
        /***********nodes************/
        var nodes = d3.selectAll('.node');
        var nodesCount = nodes._groups[0].length;
        var dataNodes = this._getDataAboutNodes();
        var simulation = d3.forceSimulation().nodes(dataNodes);
        nodes.data(dataNodes);

        simulation
            .force("charge", d3.forceManyBody().distanceMin(20))
            .force("center", d3.forceCenter(this._canvasHeight / 2, this._canvasWidth / 2))
            .force("links", linkForce)
            .force("collide", d3.forceCollide(110));

        /**
         * @function tickActions
         * @description broadcasts new calculated coordinats to visual elements
         */
        function tickActions() {
            nodes
                .attr("transform", function (d) { return "translate(" + d.x + " " + d.y + ")"; })
            //TODO remove 'rect' from select
            links
                .attr("x1", function (d) {
                    //debugger;
                    var select = d3.select('#' + d.source.id + ' rect');
                    if(!select.size()) return 0;
                    var width = select
                        .node()
                        .getBoundingClientRect()
                        .width;
                    var xDistance = d.source.x - d.target.x;
                    if (Math.abs(xDistance) <= width)
                        return d.source.x + width / 2;
                    if (d.source.x > d.target.x)
                        return d.source.x;
                    if (d.source.x < d.target.x)
                        return d.source.x + width;
                    return d.source.x;
                })
                .attr("y1", function (d) {
                    var select = d3.select('#' + d.source.id + ' rect');
                    if(!select.size()) return 0;
                    var height = select
                        .node()
                        .getBoundingClientRect()
                        .height;
                    var yDistance = d.source.y - d.target.y;
                    if (Math.abs(yDistance) <= height*2)
                        return d.source.y + height / 2;
                    if (d.source.y > d.target.y)
                        return d.source.y;
                    if (d.source.y < d.target.y)
                        return d.source.y + height;
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    var select = d3.select('#' + d.target.id + ' rect');
                    if(!select.size()) return 0;
                    var width = select
                        .node()
                        .getBoundingClientRect()
                        .width;
                    var xDistance = d.source.x - d.target.x;
                    if (Math.abs(xDistance) <= width*2)
                        return d.target.x + width / 2;
                    if (d.source.x > d.target.x)
                        return d.target.x + width;
                    if (d.source.x < d.target.x)
                        return d.target.x;
                    return d.target.x + width / 2;
                })
                .attr("y2", function (d) {
                    var select = d3.select('#' + d.target.id + ' rect');
                    if(!select.size()) return 0;
                    var height = select
                        .node()
                        .getBoundingClientRect()
                        .height;
                    var yDistance = d.source.y - d.target.y;
                    if (Math.abs(yDistance) <= height*2)
                        return d.target.y + height / 2;
                    if (d.source.y > d.target.y)
                        return d.target.y + height;
                    if (d.source.y < d.target.y)
                        return d.target.y;
                    return d.target.y;
                });
            /*we add "+20" and "+40" to center binding point of link in node*/
        }
        simulation.on("tick", tickActions);
        //simulation.on("end", function(){simulation.stop()});
    }

    this._getDataAboutNodes = function () {
        var nodesId = [];
        var nodes = d3.selectAll('.node')._groups[0];
        nodes.forEach(element => {
            let node = {};
            node.id = element.id;
            nodesId.push(node);
        });

        return nodesId;
    }

    this._getDataAboutLinks = function () {
        var linkData = [];
        var links = d3.selectAll('.link')._groups[0];
        links.forEach(element => {
            let link = {};
            link.source = element.dataset.source;
            link.target = element.dataset.target;
            linkData.push(link);
        });
        return linkData;
    }

}