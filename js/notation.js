/**
 * @class Widget
 * @param {ConceptModelElement} conceptModelElement 
 */
function Widget(conceptModelElement) {
    this._conceptModelElement = undefined;
    this.draw = function (canvas) {
        console.error('Method draw was not implemented');
    }
    this.setElement = function (cmElement) {
        this._conceptModelElement = cmElement;
    }
}
/**
 * @class ZoomableWidget
 * @extends Widget
 */
function ZoomableWidget(conceptModelElement) {
    Widget.apply(this, arguments);
    /**_zoomingViews set a visibility of any visual component
     *  of the element in every level*/
    this._zoomingViews = {
        1: {},   //the most detailed level
        2: {},   //
        3: {}    //the least detailed level 
    };
    this._currentView = 3;
    this.setView = function (viewNumber) {
        this._currentView = viewNumber;
    }
    this._getView = function () {
        return this._zoomingViews[this._currentView] ? this._zoomingViews[this._currentView] : this._zoomingViews[1];
    }
}
ZoomableWidget.prototype = Object.create(Widget.prototype);

/**
 * @class Notation - abstract factory for creating widgets in different notations
 */
function Notation() {
    /**
     * @returns {Widget}
     */
    this.createAttribute = function () {
        console.error('Method createAttribute was not implemented');
    }
    /**
     * @returns {Widget}
     */
    this.createEntityType = function () {
        console.error('Method createEntityType was not implemented');
    }
    /**
     * @returns {Widget}
     */
    this.createPredicate = function () {
        console.error('Method createPredicate was not implemented');
    }
    /**
     * @returns {Widget}
     */
    this.createInheritor = function () {
        console.error('Method createPredicate was not implemented');
    }
    /**
     * @returns {SemanticZooming}
     */
    this.createSemanticZooming = function () {
        console.error('Method craeteSemanticZooming was not implemented');
    }
}

/**
 * @class OrmNotation - factory for creating widgets in an ORM notation
 * @extends Notation
 */
function OrmNotation() {
    Notation.apply(this, arguments);
    /**
     * @returns {Widget}
     * @override
     */
    this.createAttribute = function () {
        console.error('Method createAttribute was not implemented');
    }
    /**
     * @returns {Widget}
     * @override
     */
    this.createEntityType = function () {
        var EntityTypeWidget = function () {
            ZoomableWidget.apply(this, arguments);
            this._baseSize = {
                width: 80,
                height: 40
            }
            this._sizeCoefficient = []
            this._sizeCoefficient[ImportanceLevel.EXTREMELY_IMPORTANT] = 1.5;
            this._sizeCoefficient[ImportanceLevel.VERY_IMPORTANT] = 1.3;
            this._sizeCoefficient[ImportanceLevel.QUITE_IMPORTANT] = 1.0;
            this._sizeCoefficient[ImportanceLevel.NOT_VERY_IMPORTANT] = 0.8;
            this._sizeCoefficient[ImportanceLevel.NOT_IMPORTANT] = 0.5;

            this._zoomingViews = {
                1: { RECTANGLE: true, TEXT: true },
                2: { RECTANGLE: true, TEXT: true },
                3: { RECTANGLE: true, TEXT: true },
            };
            this.onExpand = function () {
                this._conceptModelElement.expand();
            };
            this.draw = function (canvas) {
                var id = this._conceptModelElement.getId();
                var isVisible = this._conceptModelElement.isVisible();
                var isDrawn = (d3.select('#' + id).size() > 0);
                if (!isVisible && !isDrawn) {
                    return;
                }
                else if (!isVisible && isDrawn) {
                    d3.select('#' + id).remove();
                    return;
                }
                else if (isVisible) {
                    var canvas = d3.select(canvas + ' .nodes-container');
                    var g;
                    if (isDrawn) {
                        g = d3.select('#' + id);
                        g.html("");
                    }
                    else {
                        g = canvas.append("g")
                            .attr("id", id)
                            .attr("class", "node");
                    }
                    var importance = this._conceptModelElement.getImportance();
                    var name = this._conceptModelElement.name
                    var height = this._baseSize.height * this._sizeCoefficient[importance],
                        width = this._baseSize.width * this._sizeCoefficient[importance];
                    var view = this._getView();
                    if (view.RECTANGLE) {
                        g.append("rect")
                            .attr("class", "entity-node")
                            .attr("width", width)
                            .attr("height", height)
                    }
                    if (view.TEXT) {
                        g.append("text")
                            .attr("text-anchor", "middle")
                            .attr("x", width / 2)
                            .attr("y", height / 2)
                            .text(name)
                    }
                }
            }
        }
        EntityTypeWidget.prototype = Object.create(ZoomableWidget.prototype);
        return new EntityTypeWidget();
    }
    /**
     * @returns {Widget}
     * @override
     */
    this.createPredicate = function () {
        var PredicateWidget = function () {
            ZoomableWidget.apply(this, arguments);
            this._zoomingViews = {
                1: { LINE: true, TEXT: true, RECTANGLE_PAIR: true },
                2: { LINE: true, TEXT: false, RECTANGLE_PAIR: true },
                3: { LINE: true, TEXT: false, RECTANGLE_PAIR: false }
            };
        }
        PredicateWidget.prototype = Object.create(ZoomableWidget.prototype);
        return new PredicateWidget();
    }

    this.createInheritor = function () {
        var InheritorWidget = function () {
            ZoomableWidget.apply(this, arguments);
            this._zoomingViews = {
                1: { LINE: true, TRIANGLE: false },
                2: { LINE: true, TRIANGLE: true },
                3: { LINE: true, TRIANGLE: true },
            };
            this.draw = function (canvas) {
                var id = this._conceptModelElement.getId();
                var isVisible = this._conceptModelElement.isVisible();
                var isDrawn = (d3.select('#' + id).size() > 0);

                if (!isVisible && !isDrawn) {
                    return;
                }
                else if (!isVisible && isDrawn) {
                    d3.select('#' + id).remove();
                    return;
                }
                else {
                    if (!isDrawn) {
                        var svg = d3.select(canvas + " .links-container");
                        line = svg.append("line")
                            .attr("id", id)
                            .attr("data-source", this._conceptModelElement.source.getId())
                            .attr("data-target", this._conceptModelElement.target.getId())
                            .attr("stroke-width", 2)
                    }
                    var view = this._getView();
                    var linkClass = "link";
                    if (view.TRIANGLE)
                        linkClass += ' inheritor-link'
                    d3.select('#' + id).attr('class', linkClass);
                }
            }
        }
        InheritorWidget.prototype = Object.create(ZoomableWidget.prototype);
        return new InheritorWidget();
    }

    this.createSemanticZooming = function (conceptModel) {

        /**
         * @class
         * @extends SemanticZooming
         */
        var OrmSemanticZooming = function (conceptModel) {
            SemanticZooming.apply(this, arguments)

            this.applyZooming = function (level) {
                if (!this.isValidSetting(level)) return;
                var elements = conceptModel.getAllElements();
                elements.forEach(element => {
                    element.widget.setView(level)
                });
            }

            this.isValidSetting = function (level) {
                if (!Number.isInteger(level)) {
                    console.error('Semantic zooming level is not integer');
                    return false;
                }
                if (level < 1 || level > 3) {
                    console.error('Semantic zooming level must be between 1 and 3');
                    return false;
                }
                return true;
            }
        }
        OrmSemanticZooming.prototype = Object.create(SemanticZooming.prototype);

        return new OrmSemanticZooming(conceptModel);
    }
}
OrmNotation.prototype = Object.create(Notation.prototype);