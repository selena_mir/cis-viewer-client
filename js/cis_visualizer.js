/**
 * @class
 */
function CISVisualizer() {
    this.cis = new CIS();
    this._canvasHeight = 2000;
    this._canvasWidth = 2000;
    this.svg = '.canvas';
    this.canvas = this.svg + " g";
    d3.select(this.svg)
        .attr('width', this._canvasWidth)
        .attr('height', this._canvasHeight);
    this.viewerSettings = {
        semanicZoomingByImportanse: 5,
        detalizationLevel: 3,
        geometricZooming: 1
    }
    this.show = function () {
        var success = this.cis.init();
        if(!success){
            alert ("Data loading crashed. Please try again later")
        }
        d3.select('h1').text(this.cis.name);
        this.conceptModel = this.cis.conceptModel;
        //set notation
        var notation = new OrmNotation();
        this.conceptModel.applyNotation(notation);
        //apply zooming
        this._notationSemanticZooming = notation.createSemanticZooming(this.conceptModel);
        this._notationSemanticZooming.applyZooming(this.viewerSettings.detalizationLevel);
        this._semanicZoomingByImportanse = new SemanticZoomingByImportance(this.conceptModel);
        this._semanicZoomingByImportanse.applyZooming(this.viewerSettings.semanicZoomingByImportanse);

        this._layout = new Layout(this.svg, this._canvasHeight, this._canvasWidth);

        this._setGeometricZoom();
        this._setDetalizationLevel();
        this.conceptModel.draw(this.canvas);
        this._setSemanticZoom();

        this._layout.apply();
    }

    this._setGeometricZoom = function () {

        var g = d3.select(this.svg + " .main-container");

        var canvasWidth = this._canvasWidth;
        var canvasHeight = this._canvasHeight;
        var settings = this.viewerSettings;
        var scale = function (e, target) {
            var newScale = target.attr('aria-valuenow');
            var curScale = settings.geometricZooming;
            if (curScale != newScale) {
                var bRect = d3.select('.main-container').node().getBoundingClientRect();
                var newWidth = bRect.width * curScale / newScale;
                var newHeight = bRect.height * curScale / newScale;
                g.attr("transform", d3.zoomIdentity
                    //.translate(newWidth-bRect.width, newHeight - bRect.height )
                    .scale(newScale));
                settings.geometricZooming = newScale;
            }
        }
        $('.slider-geometric-zoom').on("changed.zf.slider", scale);
        $('.slider-geometric-zoom').on("moved.zf.slider", scale);
    }

    this._setSemanticZoom = function () {
        var settings = this.viewerSettings;
        var semanicZoomingByImportanse = this._semanicZoomingByImportanse;
        var conceptModel = this.conceptModel;
        var canvas = this.canvas;
        var layout = this._layout;
        var semanticZoom = function (e, target) {
            var newLevel = target.attr('aria-valuenow');
            var currentLevel = settings.semanicZoomingByImportanse;
            if (newLevel != currentLevel) {
                semanicZoomingByImportanse.applyZooming(newLevel);
                settings.semanicZoomingByImportanse = newLevel;

                //updating graph
                conceptModel.draw(canvas);
                if(newLevel < currentLevel){
                    layout.apply();
                }
            }
        }
        $('.slider-semantic-zoom').on("changed.zf.slider", semanticZoom);
    }

    this._setDetalizationLevel = function () {
        var settings = this.viewerSettings;
        var notationSemanticZooming = this._notationSemanticZooming;
        var conceptModel = this.conceptModel;
        var canvas = this.canvas;
        var layout = this._layout;
        var semanticZoom = function (e, target) {
            var newLevel = target.attr('aria-valuenow');
            var currentLevel = settings.detalizationLevel;
            if (newLevel != currentLevel) {
                notationSemanticZooming.applyZooming(parseInt(newLevel));
                settings.detalizationLevel = newLevel;
                //updating graph
                conceptModel.draw(canvas);
                //layout.apply();
            }
        }
        $('.detalization-levels').on("changed.zf.slider", semanticZoom);
    }


}

var vis = new CISVisualizer();
vis.show();