/**
 * @class
 * @param  {ConceptModel} conceptModel
 */
function SemanticZooming(conceptModel) {
    this.conceptModel = conceptModel;
    /**
     * @param  {} settings
     */
    this.applyZooming = function (settings) { }
    /**
     * @param {any} settings 
     */
    this.isValidSetting = function (settings) { }
}

/**
 * @class SemanticZoomingByImportance
 * @param {ConceptModel} conceptModel
 * @extends SemanticZooming
 */
function SemanticZoomingByImportance(conceptModel) {
    SemanticZooming.apply(this, arguments);
    /**
     * @param {number} level
     * @override
     */
    this.applyZooming = function (level) {
        if (!this.isValidSetting()) return;
        var arConcepts = this.conceptModel.conceptCollection.elements;
        arConcepts.forEach(concept => {
                concept.setVisible(concept.getImportance() >= level);
        });

    }

    /**
     * @param  {number} level - number between 1 and 5
     * @returns {boolean}
     * @override
     */
    this.isValidSetting = function (level) {
        if (Number.isInteger(level)) {
            console.error('Semantic zooming level is not integer');
            return false;
        }
        if (level < 1 || level > 5) {
            console.error('Semantic zooming level must be between 1 and 5');
            return false;
        }
        return true;
    }
}
SemanticZoomingByImportance.prototype = Object.create(SemanticZooming.prototype);