/**
 * Concept's importance levels 
 * @readonly
 * @enum
 */
const ImportanceLevel = {
    EXTREMELY_IMPORTANT: 5,
    VERY_IMPORTANT: 4,
    QUITE_IMPORTANT: 3,
    NOT_VERY_IMPORTANT: 2,
    NOT_IMPORTANT: 1,

    isLevel: function (value) {
        return Number.isInteger(value) && value <= 5 && value >= 1;
    }
};
Object.freeze(ImportanceLevel);

/**
 * 
 */
function ElementCollection() {
    this.elements = [];
    this.getById = function (id) {
        return this.elements.find(function(element){
            return element._id == id
        });
    }
    this.add = function (newElement) {
        this.elements.push(newElement);
    }
    this.size = function () {
        return this.elements.length;
    }
}

/**
 * СIS Concept Model
 * @class
 */
function ConceptModel() {

    this.conceptCollection = new ElementCollection();
    this.relationCollection = new ElementCollection();
    this.rRelationCollection = new ElementCollection();

    this.addConcept = function (concept) {
        this.conceptCollection.add(concept);
    }
    this.addRelation = function (relation) {
        this.relationCollection.add(relation);
    }
    this.addRRelation = function (rrelation) {
        this.rRelationCollection.add(rrelation);
    }
    this.draw = function (canvas) {
        var concepts = this.conceptCollection.elements;
        var relations = this.relationCollection.elements;
        var rrelations = this.rRelationCollection.elements;
        var elements = rrelations.concat(relations).concat(concepts);
        elements.forEach(element => {
            element.draw(canvas);
        });
    };
    this.applyNotation = function (notation) {
        var elements = this.getAllElements();
        elements.forEach(element => {
            element.setWidget(notation);
        });
    };
    this.getAllElements = function () {
        var concepts = this.conceptCollection.elements;
        var relations = this.relationCollection.elements;
        var rrelations = this.rRelationCollection.elements;
        var elements = concepts.concat(relations).concat(rrelations);
        return elements;
    }
}

/**
 * @class
 */
function ConceptModelElement(id) {
    /**
     * Set concept visibility
     * @type {boolean}
    */
    this._id = id;
    this._visible = true;
    this.widget = undefined;
    this.setVisible = function (value) {
        this._visible = !!value;
    }
    this.isVisible = function () {
        return this._visible;
    }
    this.setWidget = function (notationFactory) {
        Console.error('Method setWidget was not implemented');
    }
    this.draw = function (canvas) {
        this.widget.draw(canvas);
    }
    this.getId = function () {
        return this._id;
    }
}

/**
 * Concept
 * @param {string} id
 * @class
 * @extends ConceptModelElement
 */
function Concept(id, importance = ImportanceLevel.NOT_IMPORTANT, name = id) {
    ConceptModelElement.apply(this, arguments);
    this.name = name
    this.relations = [];
    if (ImportanceLevel.isLevel(importance)) {
        this._importance = importance;
    }
    else {
        this._importance = ImportanceLevel.NOT_IMPORTANT;
        console.error('Invalid importance level for ' + this._id + ' concept');
    }
    this.getImportance = function () {
        return this._importance;
    }
    this.getId = function () {
        return this._id;
    }
    this.addRelation = function (relation) {
        this.relations.push(relation);
    }
    /**
     * 
     * @param {boolean} value 
     */
    this.setVisible = function (value) {
        this._visible = !!value;
        this.relations.forEach(element => {
            element.setVisible(this._visible);
        });
    }
}
Concept.prototype = Object.create(ConceptModelElement.prototype);

/**
 * 
 * @param {string} id 
 * @param {IMPORTANCE_LEVEL} importance
 * @class
 * @extends Concept
 */
function EntityType(id, importance = ImportanceLevel.NOT_IMPORTANT, name = id) {
    Concept.apply(this, arguments);
    /**
     * Set concept expanded or collapsed
     * @type {boolean}
    */
    //this._expanded = true;

    /**
     * Set concept is activated
     * @type {boolean}
    */
    this._active = false;
    this.setWidget = function (notationFactory) {
        this.widget = notationFactory.createEntityType();
        this.widget.setElement(this); //todo call parent method for setting widget
    }
    this.expand = function () {
        this.relations.forEach(relation => {
            relation.onConceptExpand(this);
        });
    }
    this.isExpanded = function () {
        if (this.relations.length > 0) {
            this.relations.forEach(relation => {
                if (!relation.isVisible())
                    return false;
            });
        }
        return true;
    }
}
EntityType.prototype = Object.create(Concept.prototype);

/**
 * 
 * @param {string} id 
 * @param {Concept} startConcept 
 * @param {Concept} finishConcept 
 * @class
 */
function Relation(id, startConcept, finishConcept) {
    ConceptModelElement.apply(this, arguments);
    this.source = startConcept;
    startConcept.addRelation(this);
    this.target = finishConcept;
    finishConcept.addRelation(this);
    this.onConceptExpand = function (concept) {
        if (concept == this.source)
            this.target.setVisible(true);
        else if (concept == this.target)
            this.source.setVisible(true);
        this.setVisible(true);
    }
    this.setVisible = function (value) {
        if (!!!value) {
            this._visible = !!value;
        }
        else if (this.target.isVisible() && this.source.isVisible()) {
            this._visible = !!value;
        }
    }
}
Relation.prototype = Object.create(ConceptModelElement.prototype);

/**
 * @class Predicate
 * @extends Relation
 */
function Predicate(id, startConcept, finishConcept) {
    Relation.apply(this, arguments);
    this.setWidget = function (notationFactory) {
        this.widget = notationFactory.createPredicate();
    }
}
Predicate.prototype = Object.create(Relation.prototype);

/**
 * @class Inheritor
 * @extends Relation
 */
function Inheritor(id, startConcept, finishConcept) {
    Relation.apply(this, arguments);
    this.setWidget = function (notationFactory) {
        this.widget = notationFactory.createInheritor();
        this.widget.setElement(this);
    }
}
Inheritor.prototype = Object.create(Relation.prototype);

/**
 * @class CIS
 * @param {string} id 
 */
function CIS(id) {
    this._id = id;
    this._name = '';
    this.conceptModel = new ConceptModel();
    this.init = function () {
        var data = this._getData();
        if (data) {
            this._parseData(data);
        }
        else {
            return false;
        }
        return true;

    }
    /**
     * @returns [Object|false]
     */
    this._getData = function () {
        /*server request will be here,
         but now we deal with _inputData ....*/
        return (_inputData ? _inputData : false);
    }
    /**
     * 
     * @param {*} data 
     * @returns [|false]
     */
    this._parseData = function (data) {
        if (data.name)
            this.name = data.name;
        //parse CM    
        if (data.conceptModel) {
            if (data.conceptModel.concepts && Array.isArray(data.conceptModel.concepts)) {
                data.conceptModel.concepts.forEach(element => {
                    var concept = new window[element.type](element.id, element.importance, element.name)
                    this.conceptModel.addConcept(concept);
                });
            }
            if (data.conceptModel.relations && Array.isArray(data.conceptModel.relations)) {
                data.conceptModel.relations.forEach(element => {
                    var source = this.conceptModel.conceptCollection.getById(element.source);
                    var target = this.conceptModel.conceptCollection.getById(element.target);
                    if(source && target){
                    var relation = new window[element.type](element.id, source, target);
                    this.conceptModel.addRelation(relation);
                    }
                    else{
                        console.error("no source or target")
                    }
                });
            }
        }
        //parse documents

        //parse concept index
    }
}