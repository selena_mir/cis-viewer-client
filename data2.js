var _inputData = 
	{
		name: "Animals taxonomy",
		documents: [{}],
		conceptModel: {
			concepts: [
				{ id: "c1", type: "EntityType", importance: 5, name: "Животные" },
				{ id: "c2", type: "EntityType", importance: 4, name: "членистоногие" },
				{ id: "c3", type: "EntityType", importance: 4, name: "хордовые" },
				{ id: "c4", type: "EntityType", importance: 3, name: "ракообразные" },
				{ id: "c5", type: "EntityType", importance: 3, name: "насекомые" },
				{ id: "c6", type: "EntityType", importance: 3, name: "птицы" },
				{ id: "c7", type: "EntityType", importance: 3, name: "млекопитающие" },
				{ id: "c8", type: "EntityType", importance: 3, name: "рыбы" },
				{ id: "c9", type: "EntityType", importance: 2, name: "вьюрковые" },
				{ id: "c10", type: "EntityType", importance: 2, name: "соколиные" },
				{ id: "c11", type: "EntityType", importance: 2, name: "попугаеобразные" },
				{ id: "c12", type: "EntityType", importance: 2, name: "парнокопытные" },
				{ id: "c13", type: "EntityType", importance: 2, name: "хищники" },
				{ id: "c14", type: "EntityType", importance: 2, name: "грызуны" },
				{ id: "c15", type: "EntityType", importance: 2, name: "приматы" },
				{ id: "c16", type: "EntityType", importance: 1, name: "кошачьи" },
				{ id: "c17", type: "EntityType", importance: 1, name: "собачьи" },
				{ id: "c18", type: "EntityType", importance: 1, name: "гуманоиды" },
				{ id: "c19", type: "EntityType", importance: 3, name: "обезьяны" }
			],
			relations: [
				{ id: "r1", type: "Inheritor", source: "c2", target: "c1", name: "" },
				{ id: "r2", type: "Inheritor", source: "c3", target: "c1", name: "" },
				{ id: "r3", type: "Inheritor", source: "c4", target: "c2", name: "", },
				{ id: "r4", type: "Inheritor", source: "c5", target: "c2", name: "", },
				{ id: "r5", type: "Inheritor", source: "c6", target: "c3", name: "", },
				{ id: "r6", type: "Inheritor", source: "c7", target: "c3", name: "", },
				{ id: "r7", type: "Inheritor", source: "c8", target: "c3", name: "", },
				{ id: "r8", type: "Inheritor", source: "c9", target: "c6", name: "", },
				{ id: "r9", type: "Inheritor", source: "c10", target: "c6", name: "", },
				{ id: "r10", type: "Inheritor", source: "c11", target: "c6", name: "", },
				{ id: "r11", type: "Inheritor", source: "c12", target: "c7", name: "", },
				{ id: "r12", type: "Inheritor", source: "c13", target: "c7", name: "", },
				{ id: "r13", type: "Inheritor", source: "c14", target: "c7", name: "", },
				{ id: "r14", type: "Inheritor", source: "c15", target: "c7", name: "", },
				{ id: "r15", type: "Inheritor", source: "c16", target: "c13", name: "", },
				{ id: "r16", type: "Inheritor", source: "c17", target: "c13", name: "", },
				{ id: "r17", type: "Inheritor", source: "c18", target: "c15", name: "", },
				{ id: "r18", type: "Inheritor", source: "c19", target: "c16", name: "", }
			],
			conceptIndex: [
				{
					document: "",
					concept: ""
				}
			]
		}

	}