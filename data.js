var _inputData =
    {
        name: "Seasons",
        documents: [{}],
        conceptModel: {
            concepts: [{ id: "c5", type:"EntityType", importance: 3, name: "Height of Sun above Horizon" },
            { id: "c2", type:"EntityType", importance: 4, name: "Amount of Sunlight" },
                { id: "c7", type:"EntityType", importance: 2, name: "Summer" },
                { id: "c1", type:"EntityType", importance: 5, name: "Seasons" },
                
                { id: "c3", type:"EntityType", importance: 3, name: "Saesonal Temperature Variations" },
                { id: "c4", type:"EntityType", importance: 3, name: "Length of day" },
                
                { id: "c6", type:"EntityType", importance: 2, name: "Winter" },
            ],
            relations: [
                { id: "r1", type:"Inheritor", source: "c1", target: "c2", name: "are determined by",},
                { id: "r2", type:"Inheritor", source: "c2", target: "c3", name: "results in",},
                { id: "r3", type:"Inheritor", source: "c2", target: "c4", name: "is determined by", },
                { id: "r4", type:"Inheritor", source: "c2", target: "c5", name: "is determined by", },
                { id: "r5", type:"Inheritor", source: "c4", target: "c7", name: "is longer in",  },
                { id: "r6", type:"Inheritor", source: "c5", target: "c7", name: "is higher in", },
                { id: "r7", type:"Inheritor", source: "c4", target: "c6", name: "is shorter in", },
                { id: "r8", type:"Inheritor", source: "c5", target: "c6", name: "is lower in",},
            ],
        },
        conceptIndex: [
            {
                document: "",
                concept: ""
            }
        ]
    }